# ProjetoDACC_App: Cadastro de Professores em React Native

Bem-vindo ao ProjetoDACC_App! Este é um aplicativo mobile desenvolvido com React Native para gerenciamento de cadastro de professores. Aqui, você encontrará instruções sobre como configurar, instalar e executar o aplicativo.

## Índice

- [ProjetoDACC\_App: Cadastro de Professores em React Native](#projetodacc_app-cadastro-de-professores-em-react-native)
  - [Índice](#índice)
  - [Pré-requisitos](#pré-requisitos)
  - [Configuração do Ambiente](#configuração-do-ambiente)
  - [Instalação](#instalação)
  - [Execução](#execução)
  - [Funcionalidades](#funcionalidades)
  - [Estrutura do Projeto](#estrutura-do-projeto)
  - [Suporte](#suporte)
  - [Contribuição](#contribuição)
  - [Licença](#licença)

## Pré-requisitos

Antes de iniciar, certifique-se de:

- Ter o [Node.js](https://nodejs.org/) instalado.
- Ter o [React Native CLI](https://reactnative.dev/docs/environment-setup) configurado.
- Ter um emulador Android/iOS ou um dispositivo físico para execução.

## Configuração do Ambiente

1. **Node.js e npm**: React Native é construído com JavaScript e requer o Node.js para execução. Se você ainda não tem o Node.js instalado, siga para [nodejs.org](https://nodejs.org/) e baixe a versão recomendada.

2. **React Native CLI**: Siga as [instruções oficiais](https://reactnative.dev/docs/environment-setup) para configurar o ambiente React Native em seu sistema operacional.

## Instalação

1. **Clone este repositório**:

```bash
git clone https://link-do-seu-repositório.git
```

2. **Navegue até o diretório do projeto**:

```bash
cd caminho-para-o-diretório-do-ProjetoDACC_App
```

3. **Instale as dependências**:

```bash
npm install
```

## Execução

- Para Android:

```bash
npx react-native run-android
```

- Para iOS (apenas macOS):

```bash
npx react-native run-ios
```

## Funcionalidades

- **Listagem de Professores**: Veja todos os professores cadastrados.
- **Adição de Professores**: Adicione novos professores utilizando um formulário prático.
- **Edição de Professores**: Edite informações dos professores cadastrados.
- **Exclusão de Professores**: Remova professores da lista quando necessário.

## Estrutura do Projeto

O projeto segue uma estrutura modular:

- `src/`: Contém todo o código fonte da aplicação.
- `src/components/`: Guarda os componentes React Native.
- `src/api/`: Funções para interagir com a API REST.
- `src/models/`: Define os modelos de dados usados no projeto.

## Suporte

Se você encontrar problemas ou tiver dúvidas sobre o projeto, abra uma "issue" no GitHub ou entre em contato diretamente.

## Contribuição

Contribuições são sempre bem-vindas! Se você quiser contribuir, faça um "fork" do repositório, crie uma "branch" para sua funcionalidade ou correção e envie um "pull request".

## Licença

Este projeto está sob a licença MIT. Veja o arquivo `LICENSE` para mais detalhes.
