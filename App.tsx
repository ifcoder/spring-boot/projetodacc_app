import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import CadastroProfessores from './src/components/CadastroProfessores/CadastroProfessores';

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <CadastroProfessores />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default App;
