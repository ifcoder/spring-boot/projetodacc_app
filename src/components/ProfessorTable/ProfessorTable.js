import React from 'react';
import { FlatList, TouchableOpacity } from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome5'; // Importando ícones do pacote.
import Icon from 'react-native-vector-icons/FontAwesome';

import { Row, Cell, Actions, CellChar } from './ProfessorTableStyles';

const ProfessorTable = ({ professores, onEdit, onDelete }) => {
    const renderProfessorItem = ({ item }) => {
        return (
            <Row>
                <Cell>{item.nome}</Cell>
                <Cell>{item.cpf}</Cell>
                <CellChar>{item.sexo}</CellChar>
                <CellChar>{item.idade}</CellChar>                
                <Actions>
                    <TouchableOpacity onPress={() => onEdit(item)}>
                        <Icon name="edit" size={20} color="rgb(146, 176, 236)" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onDelete(item.cpf)}>
                        <Icon name="trash" size={20} color="rgb(219, 67, 125)" />
                    </TouchableOpacity>
                </Actions>
            </Row>
        );
    };

    return (
        <FlatList
            data={professores}
            renderItem={renderProfessorItem}
            keyExtractor={(item) => item.cpf}
        />
    );
};

export default ProfessorTable;
