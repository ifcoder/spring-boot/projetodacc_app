import styled from 'styled-components/native';

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  border-bottom-width: 1px;
  border-color: #ccc;
`;

export const Cell = styled.Text`
  flex: 1;
`;

export const CellChar = styled.Text`
   width: 25px;
`;

export const Actions = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 50px;
`;
