import firestore from '@react-native-firebase/firestore';

import { firebase } from '@react-native-firebase/app';
firebase.initializeApp();
const db = firestore();


export const getProfessores = async () => {
  try {
    const snapshot = await db.collection('professores').get();
    return snapshot.docs.map(doc => ({ cpf: doc.cpf, ...doc.data() }));
  } catch (error) {
    console.error("Erro ao buscar professores:", error);
    return [];
  }
};

export const createProfessor = async (professor) => {
  try {
    const response = await db.collection('professores').add(professor);
    return { id: response.id, ...professor };
  } catch (error) {
    console.error("Erro ao criar professor:", error);
    return null;
  }
};

export const updateProfessor = async (cpf, professor) => {
   try{ 
    const querySnapshot = await db.collection('professores').where('cpf', '==', cpf).get();
    const batch = db.batch();
    
    querySnapshot.forEach((doc) => {
      batch.update(doc.ref, professor);
    });

    await batch.commit();
    console.log('Professor atualizado com sucesso');
    return professor;
  } catch (error) {
    console.error('Erro ao atualizar documentos', error);
    return null;
  }  
   
};

export const deleteProfessor = async (cpf) => {
    try {
        const querySnapshot = await db.
        collection('professores').
        where('cpf', '==', cpf).
        get();
        const batch = db.batch();
        
        querySnapshot.forEach((doc) => {
          batch.delete(doc.ref);
        });
    
        await batch.commit();
        console.log('Professor deletados com sucesso');
        return true;
      } catch (error) {
        console.error('Erro ao deletar documentos', error);
        return false;
      }
};
