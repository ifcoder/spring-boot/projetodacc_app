import firestore from '@react-native-firebase/firestore';

import { firebase } from '@react-native-firebase/app';


class ProfessoresFireBase {
    constructor() {
      firebase.initializeApp();
      this.db = firestore();
    }
  
    async getProfessores() {
      try {
        const snapshot = await this.db.collection('professores').get();
        return snapshot.docs.map(doc => ({ cpf: doc.cpf, ...doc.data() }));
      } catch (error) {
        console.error("Erro ao buscar professores:", error);
        return [];
      }
    }
  
    async createProfessor(professor) {
      try {
        const response = await this.db.collection('professores').add(professor);
        return { id: response.id, ...professor };
      } catch (error) {
        console.error("Erro ao criar professor:", error);
        return null;
      }
    }
  
    async updateProfessor(cpf, professor) {
      try{ 
        const querySnapshot = await this.db.collection('professores').where('cpf', '==', cpf).get();
        const batch = this.db.batch();
        
        querySnapshot.forEach((doc) => {
          batch.update(doc.ref, professor);
        });
    
        await batch.commit();
        console.log('Professor atualizado com sucesso');
        return professor;
      } catch (error) {
        console.error('Erro ao atualizar documentos', error);
        return null;
      }  
    }
  
    async deleteProfessor(cpf) {
      try {
        const querySnapshot = await this.db.collection('professores').where('cpf', '==', cpf).get();
        const batch = this.db.batch();
        
        querySnapshot.forEach((doc) => {
          batch.delete(doc.ref);
        });
    
        await batch.commit();
        console.log('Professor deletados com sucesso');
        return true;
      } catch (error) {
        console.error('Erro ao deletar documentos', error);
        return false;
      }
    }


    async findProfessorByAttribute(attribute, value) {
      try {
        const querySnapshot = await this.db.collection('professores').where(attribute, '==', value).get();
        return querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
      } catch (error) {
        console.error("Erro ao buscar professor:", error);
        return [];
      }
    }
  }
  
  export default ProfessoresFireBase;
  