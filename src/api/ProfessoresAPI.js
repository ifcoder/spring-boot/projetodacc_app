import axios from 'axios';

class ProfessoresAPI {
    constructor() {
        const IP_SERVIDOR = '192.168.0.112';
        const API_URL = 'http://'+IP_SERVIDOR+':8080'; // Ajuste para a URL da sua API
        //const API_URL = 'http://localhost:8080'; // Ajuste para a URL da sua API
    }
  
    async getProfessores() {
        try {
            const response = await axios.get(`${API_URL}/professores/`);
            return response.data;
          } catch (error) {
            console.error("Erro ao buscar professores:", error);
            return [];
          }
    }
  
    async createProfessor(professor) {
        try {
            const response = await axios.post(`${API_URL}/professores/`, professor);
            return response.data;
          } catch (error) {
            console.error("Erro ao criar professor:", error);
            return null;
          }
    }
  
    async updateProfessor(cpf, professor) {
        try {
            const response = await axios.put(`${API_URL}/professores/${id}`, professor);
            return response.data;
          } catch (error) {
            console.error("Erro ao atualizar professor:", error);
            return null;
          }
    }
  
    async deleteProfessor(cpf) {
        try {
            await axios.delete(`${API_URL}/professores/${cpf}`);
            return true;
          } catch (error) {
            console.error("Erro ao deletar professor:", error);
            return false;
          }
    }
  }
  
  export default ProfessoresAPI;
  