// src/components/CadastroProfessores.js
import React, { useState, useEffect } from "react";
import { View, Text, Button, ScrollView, Modal } from "react-native";
import ProfessorTable from "../ProfessorTable/ProfessorTable";
import ProfessorForm from "../ProfessorForm/ProfessorForm";
//import { getProfessores, createProfessor, updateProfessor, deleteProfessor } from '../api/professoresAPI';
import {
  getProfessores,
  createProfessor,
  updateProfessor,
  deleteProfessor,
} from "../../api/professoresFire";
import ProfessoresFireBase from "../../api/ProfessoresFireBase";

import styles from "./CadastroProfessoresStyles";
import { Alert } from "react-native";

const CadastroProfessores = () => {
  const [professores, setProfessores] = useState([]);
  const [editingProfessor, setEditingProfessor] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const professoresFireBase = new ProfessoresFireBase();

  useEffect(() => {
    const fetchData = async () => {
      const data = await getProfessores();
      setProfessores(data);
    };

    fetchData();
  }, []);

  const handleAddOrUpdate = async (professor) => {
    try {
      if (editingProfessor) {
        const updatedProfessor = await professoresFireBase.updateProfessor(
          editingProfessor.cpf,
          professor
        );

        setProfessores(
          professores.map((p) =>
            p.cpf === editingProfessor.cpf ? updatedProfessor : p
          )
        );
        setEditingProfessor(null);
      } else {
        const newProfessor = await professoresFireBase.createProfessor(
          professor
        );
        if (newProfessor) {
          setProfessores([...professores, newProfessor]);
        }
      }
      setIsModalOpen(false);
      setErrorMessage(null);
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  const handleEdit = (professor) => {
    setEditingProfessor(professor);
    setIsModalOpen(true);
  };

  const handleDelete = async (cpf) => {    
    Alert.alert(
      "Confirmação",
      "Tem certeza que deseja excluir este professor?",
      [
        {
          text: "Cancelar",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: async () => {
            const success = await professoresFireBase.deleteProfessor(cpf);
            if (success) {
              setProfessores(professores.filter((p) => p.cpf !== cpf));
            } else {
              console.error("Erro ao deletar o professor.");
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>Cadastro de Professores</Text>

      <Button
        title="Adicionar Professor"
        onPress={() => setIsModalOpen(true)}
      />

      {isModalOpen && (
        <Modal
          animationType="slide"
          transparent={false}
          visible={isModalOpen}
          onRequestClose={() => setIsModalOpen(false)}
        >
          <View style={styles.modalContent}>
            <ProfessorForm
              onSubmit={handleAddOrUpdate}
              professor={editingProfessor}
            />
            <Button title="Fechar" onPress={() => setIsModalOpen(false)} />
          </View>
        </Modal>
      )}

      <ProfessorTable
        professores={professores}
        onEdit={handleEdit}
        onDelete={handleDelete}
      />

      <View style={styles.rodape}>
        <Text>Total de Professores: {professores.length}</Text>
      </View>
    </ScrollView>
  );
};

export default CadastroProfessores;
