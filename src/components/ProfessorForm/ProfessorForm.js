// src/components/ProfessorForm.js
import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';

import Professor from '../../models/Professor';

const ProfessorForm = ({ onSubmit, professor }) => {    
    const [profData, setProfData] = useState(professor || new Professor());

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(profData);
        setProfData(new Professor());
    };

    return (
        <View style={styles.formContainer}>
            <TextInput
                style={styles.input}
                placeholder="Nome"
                value={profData.nome}
                onChangeText={(text) => setProfData({ ...profData, nome: text })}
            />

            <TextInput
                style={styles.input}
                placeholder="Sexo"
                value={profData.sexo}
                onChangeText={(text) => setProfData({ ...profData, sexo: text })}
            />

            <TextInput
                style={styles.input}
                placeholder="Idade"
                keyboardType="numeric"
                value={profData.idade.toString()}
                onChangeText={(value) => {
                    const parsedValue = parseInt(value);
                    setProfData({ ...profData, idade: isNaN(parsedValue) ? '' : parsedValue });
                  }}
            />

            <TextInput
                style={styles.input}
                placeholder="CPF"
                value={profData.cpf}
                onChangeText={(text) => setProfData({ ...profData, cpf: text })}
            />

            <Button title={professor ? 'Atualizar' : 'Adicionar'} onPress={handleSubmit} />
        </View>
    );
};

const styles = StyleSheet.create({
    formContainer: {
        padding: 20
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
        paddingLeft: 10,
        borderRadius: 5
    }
});

export default ProfessorForm;
