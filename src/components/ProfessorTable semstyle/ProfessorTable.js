// src/components/ProfessorTable.js
import React from 'react';
import { View, Text, FlatList, Button, StyleSheet } from 'react-native';

const ProfessorTable = ({ professores, onEdit, onDelete }) => {
    const renderProfessorItem = ({ item }) => {
        return (
            <View style={styles.row}>
                <Text style={styles.cell}>{item.nome}</Text>
                <Text style={styles.cell}>{item.sexo}</Text>
                <Text style={styles.cell}>{item.idade}</Text>
                <Text style={styles.cell}>{item.cpf}</Text>
                <View style={styles.actions}>
                    <Button title="Editar" onPress={() => onEdit(item)} />
                    <Button title="Deletar" onPress={() => onDelete(item.cpf)} />
                </View>
            </View>
        );
    };

    return (
        <FlatList
            data={professores}
            renderItem={renderProfessorItem}
            keyExtractor={(item) => item.cpf}
        />
    );
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: '#ccc'
    },
    cell: {
        flex: 1
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 150
    }
});

export default ProfessorTable;
